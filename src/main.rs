extern crate kuchiki;

use kuchiki::traits::*;

use futures::{stream, StreamExt}; // 0.3.5
use reqwest::Client; // 0.10.6
use tokio; // 0.2.21, features = ["macros"]

const CONCURRENT_REQUESTS: usize = 2;

fn check_css_selector(node_document: kuchiki::NodeRef, css_selector: &str) -> bool {
    return match match node_document.select_first(css_selector) {
        Ok(x) => x,
        Err(_) => return false, // Nodo vuoto, nessun elemento trovato
    }
    .as_node()
    .first_child()
    .unwrap()
    .as_text()
    {
        Some(_) => true,
        None => {
            eprintln!("Elemento non trovato");
            false
        }
    };
}

fn parse_local_file() {
    let available_selector = ".productDetail .ArgAvailability span.availabilityInfo";

    let files_path = [
        "HTMLs/segway-segway-e-scooter-max-g30-monopattino-elettrico-10865586.html",
        "HTMLs/segway-ninebot-e22e-escooter-10966761.html",
    ];

    for file_path in files_path.iter() {
        let file = match std::fs::read_to_string(file_path) {
            Ok(file) => file,
            Err(e) => {
                println!("{:?}", e);
                continue;
            }
        };

        println!("Parsing file {:?}", file_path);

        let document = kuchiki::parse_html().one(file);

        if check_css_selector(document, available_selector) {
            println!("Prodotto disponibile!");
        } else {
            println!("Ancora nulla")
        }
    }
}

async fn parse_online_link() -> Result<(), Box<dyn std::error::Error>> {
    let client = Client::new();

    let available_selector = ".productDetail .ArgAvailability span.availabilityInfo";

    let urls = vec![
        "https://www.sportler.com/it/p/segway-segway-e-scooter-max-g30-monopattino-elettrico-10865586",
        "https://www.sportler.com/it/p/segway-ninebot-e22e-escooter-10966761?filterFarbe=Grey",
    ];

    let bodies = stream::iter(urls)
        .map(|url| {
            println!("Processing URL {:?}", url);
            let client = &client;
            async move {
                let resp = client.get(url).send().await?;
                resp.text().await
            }
        })
        .buffer_unordered(CONCURRENT_REQUESTS);

    bodies
        .for_each(|b| async {
            match b {
                Ok(b) => {
                    let document = kuchiki::parse_html().one(b);

                    if check_css_selector(document, available_selector) {
                        println!("Prodotto disponibile!");
                    } else {
                        println!("Ancora nulla")
                    }
                }
                Err(e) => eprintln!("Errore nella richiesta: {}", e),
            }
        })
        .await;

    return Ok(());
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    parse_local_file();
    parse_online_link().await?;
    return Ok(());
}
